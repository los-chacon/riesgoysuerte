#Elementos de Computación.
#Proyecto #1: Riesgo y suerte.
#Inicio del proyecto: 24/04/2021.
#Fin del proyecto: 01/05/2021.
#Autores: Marypaz Chacón y Darío Chacón. 
#Entradas: enter para iniciar el juego, cuantos jugadores son (2-4), nombre de cada uno de los jugadores, introduce el número que se desea jugar.
#Salidas: si falló el intento, si acertó en el intento, si ingreso un valor fuera de rango, despliegue de las reglas, cantidad de veces que ha ganado y cantidad de veces que ha fallado (penaltis).
#Limitaciones: el número de rango es de 1-20, cada rango tiene que tener una diferencia mínima 4 y máxima 8 entre ambos valores, cantidad de jugadores entre (2-4), solo se aceptan números positivos, los nombres de los jugadores solo pueden tener letras no números. 
#                                        Generalidades del juego: 
#El juego despliega las reglas, luego pide la cantidad de jugadores que van a participar, posterior a esto pide que se ingrese el nombre de los jugadores con su respectiva limitación.
#Al cumplir todos estos aspectos inicia el juego, el sistema escoge un sub rango aleatorio con sus respectivas limitaciones por cada partida dentro del rango determinado (1-20), y dentro de este rango escoge un número aleatorio por cada uno de los jugadores.
#A la hora de acertar, se despliega el mensaje "Acertaste", y si fallas se despliega el mensaje "Fallaste" y continua la ronda. 
#El jugador pierde si falla 2 veces consecutivas o si falla 3 veces en todo el juego.
#El objetivo del juego es conseguir 6 aciertos antes que los demás jugadores o acertar 3 veces consecutivas para ganar.

#Con esto se importa la libreta utilizada en el programa, la libreta random
import random

#con todos estos se validan las variables utilizadas a lo largo del programa y guarda cada uno de los penaltis tanto consecutivo y normal, hasta los aceirtos consecutivos y normales de cada uno de los jugadores 
#DatosJugador 1 
penaltiConsecutivoJugador1 = 0 
penaltiJugador1= 0 
cantidadAciertoConsecutivoJugador1 = 0
cantidadAciertosJugador1 = 0 
#DatosJugador 2
penaltiConsecutivoJugador2 = 0 
penaltiJugador2= 0 
cantidadAciertoConsecutivoJugador2 = 0
cantidadAciertosJugador2 = 0
#DatosJugador 3
penaltiConsecutivoJugador3 = 0 
penaltiJugador3= 0 
cantidadAciertoConsecutivoJugador3 = 0
cantidadAciertosJugador3 = 0
#DatosJugador 4
penaltiConsecutivoJugador4 = 0 
penaltiJugador4= 0 
cantidadAciertoConsecutivoJugador4 = 0
cantidadAciertosJugador4 = 0

#Esta variable guarda el numero del jugador actual 
jugadorActual = 0

#Esta variable guarda si el jugador sigue en el juego 
jugador1 = ""   
jugador2 = ""
jugador3 = ""
jugador4 = ""

#Estas variables guarda los nombres asignados a los jugadores 
nombreJugador1 = ""   
nombreJugador2 = ""
nombreJugador3 = ""
nombreJugador4 = ""

#Estas variables guarda los números que pueden estar en el rango 
num1 = 0 
num2 = 0 
num3 = 0 
num4 = 0 
num5 = 0 
num6 = 0 
num7 = 0 
num8 = 0

#Con esta variable se guarda la cantidad de rango a utilizar 
cantidadRango = 0

#Con esta variable se guardan la cantidad de jugadores
jugadores = 0

#Con estas variables se guarda el número maximo o número minimo que puede obtar el rango 
rangoMax = 0
rangoMin = 0

#
ganador = False

#
ganadorUser = 0


def reglas (): 
    #Esta función llama cada una de las reglas que van a ser utilizadas en el desarrollo del juego y que deben seguirse
    print  ("\n")
    print ("                                                     Reglas del Juego                                                                   ")
    print ("\n")
    print ("1. El juego se desprende del concepto del azar y la suerte que tenemos como individuos. El objetivo es conseguir 6 aciertos antes que los demás jugadores o acertar 3 veces consecutivas.")
    print ("2. En caso de introducir un valor fuera del rango que se despliega se reduce en 1 la cantidad de aciertos y se toma como un penalti.")
    print ("3. Dos penaltis consecutivos te hacen perder el juego.")
    print ("4. Tres penaltis en total te hacen perder el juego.")
    print ("5. En ambos casos en los cuales un jugador pierda el juego se debe saltar su turno por lo que no se solicitará a ese jugador más intentos.")
    print ("6. Los rangos van de 1-20 por lo que puede tomar cualquier subrango")
    print ("7. Solo se aceptan números positivos.")
    print ("8. Solo se aceptan nombres de jugadores con letras (sin números)")
    print ("\n")

#Esta funcion es la que desarrolla la cantidad de jugadores que serán utilizados en el juego 
def cantidadJugadores (): 
    #esta es la variable de jugadores, esta junto a "global" ya que necesitabamos una forma de como hacer accesible la variable en todo el juego 
    global jugadores
    #Llama al usuario a digitar la cantidad de jugadores que van a participar
    cantidad = input ("Digite el número de jugadores  ") 
    #Con 
    resultadoValidarSiEsNumero = validarNumeros(cant)
  
    if resultadoValidarSiEsNumero == True:
        jugadores = int(cantidad)
        if jugadores >= 2 and jugadores <= 4: 
            if jugadores == 2: 
                nombreJugadores (jugadores)
            elif jugadores ==3: 
                nombreJugadores (jugadores) 
            else: 
                jugadores ==4
                nombreJugadores (jugadores)
        else:
            print ("Digite otro valor  ")
            cantidadJugadores ()
    else:
        print ("Error: valor debe ser un número entero")
        print ("Digite otro valor  ")
        cantidadJugadores ()

def validarNumeros(numero):
    esValido = False
    try:
        isNumber = int(numero)
        esValido = True
        return esValido
    except Exception:
        esValido = False
        return esValido 

def validarTextos(texto):
    #inicia la variable en true el resultado
    esValido = True
    # recorre el texto caracter por caracter
    for caracter in texto:
        #intenta convertir el caracter en un int
        try:
            #si logra convertirlo pone la variable en false y devulve a la otra funcion
            isNumber = int(caracter)
            esValido = False
            return esValido
        except Exception:
            #si no sigue recorriendo con la variable en true
            esValido = True
    return esValido
    
def obtenerNombreJugadorUno():
    global jugador1, nombreJugador1
    jugador1= input("Digite el nombre del jugador 1  ")
    if validarTextos(jugador1) == True:
        nombreJugador1 = jugador1
    else:
        print ("Error: valor debe ser un texto, intente nuevamente")
        obtenerNombreJugadorUno()

def obtenerNombreJugadorDos():
    global jugador2, nombreJugador2
    jugador2= input("Digite el nombre del jugador 2  ")
    if validarTextos(jugador2) == True:
        nombreJugador2 = jugador2
    else:
        print ("Error: valor debe ser un texto, intente nuevamente")
        obtenerNombreJugadorDos()

def obtenerNombreJugadorTres():
    global jugador3, nombreJugador3
    jugador3= input("Digite el nombre del jugador 3  ")
    if validarTextos(jugador3) == True:
        nombreJugador3 = jugador3
    else:
        print ("Error: valor debe ser un texto, intente nuevamente")
        obtenerNombreJugadorTres()

def obtenerNombreJugadorCuatro():
    global jugador4, nombreJugador4
    jugador4= input("Digite el nombre del jugador 4  ")
    if validarTextos(jugador4) == True:
        nombreJugador4 = jugador4
    else:
        print ("Error: valor debe ser un texto, intente nuevamente")
        obtenerNombreJugadorCuatro()

def nombreJugadores (jugadores): 
    
    if jugadores >= 2 and jugadores <=4:
        if jugadores == 2:
            obtenerNombreJugadorUno()
            obtenerNombreJugadorDos()
            calcularRango()
        elif jugadores == 3:
            obtenerNombreJugadorUno()
            obtenerNombreJugadorDos()
            obtenerNombreJugadorTres()
            calcularRango()
        else:
            obtenerNombreJugadorUno()
            obtenerNombreJugadorDos()
            obtenerNombreJugadorTres()
            obtenerNombreJugadorCuatro()
            calcularRango()
    else: 
        print ("Digite otra cantidad de jugadores  ")
        
def calcularRango ():
    global num1
    global num2
    global num3
    global num4
    global num5
    global num6
    global num7
    global num8
    global cantidadRango, rangoMax, rangoMin
    #Diciendo que el largo del rango puede ser 4 digitos minimo y 8 digitos máximo
    cantidadRango = random.randint (4,8)
    #se saca este calculo para validar que el numero final del rango no va a sobrepasar el numero 20
    numeroMaximo = 20 - cantidadRango
    #se calcula de manera random el numero inical del rango entre 1 y el numero maximo 
    numeroInicial = random.randint (1,numeroMaximo)
    #Se inician las variables posibles a utilizar 
  
    if cantidadRango == 4:
        num1 = numeroInicial
        num2 = numeroInicial + 1
        num3 = numeroInicial + 2
        num4 = numeroInicial + 3
        rangoMin = num1
        rangoMax = num4
        print("******** El rango de la partida es:",str(num1),"...",str(num4)," *********")
        inicioPartidas()
    elif cantidadRango == 5:
        num1 = numeroInicial
        num2 = numeroInicial + 1
        num3 = numeroInicial + 2
        num4 = numeroInicial + 3 
        num5 = numeroInicial + 4
        rangoMin = num1
        rangoMax = num5
        print("******** El rango de la partida es:",str(num1)+"..."+str(num5)," *********")
        inicioPartidas()
    elif cantidadRango == 6:
        num1 = numeroInicial
        num2 = numeroInicial + 1
        num3 = numeroInicial + 2
        num4 = numeroInicial + 3
        num5 = numeroInicial + 4
        num6 = numeroInicial + 5
        rangoMin = num1
        rangoMax = num6
        print("******** El rango de la partida es:",str(num1),"...",str(num6)," *********")
        inicioPartidas()
    elif cantidadRango == 7:
        num1 = numeroInicial
        num2 = numeroInicial + 1
        num3 = numeroInicial + 2
        num4 = numeroInicial + 3
        num5 = numeroInicial + 4
        num6 = numeroInicial + 5
        num7 = numeroInicial + 6
        rangoMin = num1
        rangoMax = num7
        print("******** El rango de la partida es:",str(num1),"...",str(num7)," *********")
        inicioPartidas()
    else: 
        num1 = numeroInicial
        num2 = numeroInicial + 1
        num3 = numeroInicial + 2
        num4 = numeroInicial + 3
        num5 = numeroInicial + 4
        num6 = numeroInicial + 5
        num7 = numeroInicial + 6
        num8 = numeroInicial + 7
        rangoMin = num1
        rangoMax = num8
        print("******** El rango de la partida es:",str(num1),"...",str(num8)," *********")
        inicioPartidas()
       
def calcularNumeroGanador (numDigitado):
    global num1, num2, num3, num4, num5, num6, num7, num8, cantidadRango
    numGanador = 0 
    if cantidadRango == 4: 
    #El numero ganador llama de manera random a los numeros dentro rango de 4 numeros 
        numGanador = random.randint(num1,num4)
        return numGanador
    elif cantidadRango == 5:
    #El numero ganador llama de manera random a los numeros dentro rango de 5 numeros |
        numGanador = random.randint(num1,num5)
        return numGanador
    elif cantidadRango == 6:
    #El numero ganador llama de manera random a los numeros dentro rango de 6 numeros 
        numGanador = random.randint(num1,num6)
        return numGanador
    elif cantidadRango == 7:
    #El numero ganador llama de manera random a los numeros dentro rango de 7 numeros 
        numGanador = random.randint(num1,num7)
        return numGanador
    else:
        #El numero ganador llama de manera random a los numeros dentro rango de 8 numeros 
        numGanador = random.randint(num1,num8)
        return numGanador
    
def validarJugador1Ganar():
    global ganador,penaltiConsecutivoJugador1,cantidadAciertoConsecutivoJugador1,jugador1,cantidadAciertosJugador1

    penaltiConsecutivoJugador1 = 0
    cantidadAciertoConsecutivoJugador1 = cantidadAciertoConsecutivoJugador1+1
    cantidadAciertosJugador1 = cantidadAciertosJugador1+1

    print("Cantidad de aciertos consecutivos del jugador 1:",str(cantidadAciertoConsecutivoJugador1),"   ""Cantidad de aciertos del jugador 1:", str(cantidadAciertosJugador1))
    if cantidadAciertoConsecutivoJugador1 == 3:
        ganador = True
        print (nombreJugador1,": ha ganado el juego con 3 aciertos consecutivos")
        print ("          ¡Haz ganado, felicidades!          ")
        return
    elif cantidadAciertosJugador1 == 6:
        ganador = True
        print (nombreJugador1,": ha ganado el juego con 6 aciertos en total")
        print ("          ¡Haz ganado, felicidades!          ")  
        return
    else:
        print (" El jugador 1: ""¡", nombreJugador1,", haz acertado, felicidades! ") 
        return

def validarJugador2Ganar():
    global ganador,penaltiConsecutivoJugador2,cantidadAciertoConsecutivoJugador2,jugador2,cantidadAciertosJugador2

    penaltiConsecutivoJugador2 = 0
    cantidadAciertoConsecutivoJugador2 = cantidadAciertoConsecutivoJugador2+1
    cantidadAciertosJugador2 = cantidadAciertosJugador2+1

    print("Cantidad de aciertos consecutivos del jugador 2:" ,str(cantidadAciertoConsecutivoJugador2),"   ""Cantidad de aciertos del jugador 2:", str(cantidadAciertosJugador2))
    if cantidadAciertoConsecutivoJugador2 == 3:
        ganador = True
        print (nombreJugador2,": ha ganado el juego con 3 aciertos consecutivos")
        print ("          ¡Haz ganado, felicidades!          ")
        return
    elif cantidadAciertosJugador2 == 6:
        ganador = True
        print (nombreJugador2,": ha ganado el juego con 6 aciertos en total")
        print ("          ¡Haz ganado, felicidades!          ")  
        return
    else:
        print (" El jugador 2: ""¡",nombreJugador2,", haz acertado, felicidades! ") 
        return          

def validarJugador3Ganar():
    global ganador,penaltiConsecutivoJugador3,cantidadAciertoConsecutivoJugador3,jugador3,cantidadAciertosJugador3

    penaltiConsecutivoJugador3 = 0
    cantidadAciertoConsecutivoJugador3 = cantidadAciertoConsecutivoJugador3+1
    cantidadAciertosJugador3 = cantidadAciertosJugador3+1

    print("Cantidad de aciertos consecutivos del jugador 3:",str(cantidadAciertoConsecutivoJugador3),"   ""Cantidad de aciertos del jugador 3:", str(cantidadAciertosJugador3))
    if cantidadAciertoConsecutivoJugador3 == 3:
        ganador = True
        print (nombreJugador3,": ha ganado el juego con 3 aciertos consecutivos")
        print ("          ¡Haz ganado, felicidades!          ")
        return
    elif cantidadAciertosJugador3 == 6:
        ganador = True
        print (nombreJugador3,": ha ganado el juego con 6 aciertos en total")
        print ("          ¡Haz ganado, felicidades!          ")  
        return
    else:
        print (" El jugador 3: ""¡",nombreJugador3,", haz acertado, felicidades! ")
        return
           
def validarJugador4Ganar():
    global ganador,penaltiConsecutivoJugador4,cantidadAciertoConsecutivoJugador4,jugador4,cantidadAciertosJugador4

    penaltiConsecutivoJugador4 = 0
    cantidadAciertoConsecutivoJugador4 = cantidadAciertoConsecutivoJugador4+1
    cantidadAciertosJugador4 = cantidadAciertosJugador4+1

    print("Cantidad de aciertos consecutivos del jugador 4:",str(cantidadAciertoConsecutivoJugador4),"   ""Cantidad de aciertos consecutivos del jugador 3:",str(cantidadAciertosJugador4))
    if cantidadAciertoConsecutivoJugador4 == 3:
        ganador = True
        print (nombreJugador4,": ha ganado el juego con 3 aciertos consecutivos")
        print ("          ¡Haz ganado, felicidades!          ")
        return
    elif cantidadAciertosJugador4 == 6:
        ganador = True
        print (nombreJugador4,": ha ganado el juego con 6 aciertos en total")
        print ("          ¡Haz ganado, felicidades!          ")  
        return
    else:
        print (" El jugador 4: ""¡",nombreJugador4,", haz acertado, felicidades! ")
        return

def validarJugador1Perder(numDigitado):
    global penaltiConsecutivoJugador1, cantidadAciertoConsecutivoJugador1,penaltiJugador1,jugador1
    penaltiConsecutivoJugador1 = penaltiConsecutivoJugador1+1
    cantidadAciertoConsecutivoJugador1 = 0
    penaltiJugador1 = penaltiJugador1+1
    global rangoMin, rangoMax

    print("Cantidad de penaltis consecutivos del jugador 1:",str(penaltiConsecutivoJugador1),"   ""Cantidad de penaltis del jugador 1:", str(penaltiJugador1))
    if penaltiConsecutivoJugador1 == 2:
        jugador1 = ""
        #devolver la variable del nombre a "" 
        print (nombreJugador1,": ha perdido el juego con dos penaltis consecutivos")
        print ("          ¡Estas eliminado!           ")
        return
    elif penaltiJugador1 == 3 :
        jugador1 = ""
        print (nombreJugador1,": ha perdido el juego con 3 penaltis en total ")
        print ("          ¡Estas eliminado!           ")
        return
    elif numDigitado<rangoMin or numDigitado>rangoMax :
        print (" El jugador 1: ""¡",nombreJugador1,", haz fallado por número fuera de rango, no te rindas! ") 
    else:
        print (" El jugador 1: ""¡",nombreJugador1,", haz fallado, no te rindas! ")  
        return

def validarJugador2Perder(numDigitado):
    global penaltiConsecutivoJugador2, cantidadAciertoConsecutivoJugador2,penaltiJugador2,jugador2
    penaltiConsecutivoJugador2 = penaltiConsecutivoJugador2+1
    cantidadAciertoConsecutivoJugador2 = 0
    penaltiJugador2 = penaltiJugador2+1
    global rangoMin, rangoMax

    print("Cantidad de penaltis consecutivos del jugador 2:",str(penaltiConsecutivoJugador2),"   ""Cantidad de penaltis del jugador 2:", str(penaltiJugador2))
    if penaltiConsecutivoJugador2 == 2:
        jugador2 = ""
        #devolver la variable del nombre a "" 
        print (nombreJugador2,": ha perdido el juego con dos penaltis consecutivos")
        print ("          ¡Estas eliminado!           ")
        return
    elif penaltiJugador2 == 3 :
        jugador2 = ""
        print (nombreJugador2,": ha perdido el juego con 3 penaltis en total ")
        print ("          ¡Estas eliminado!           ")
        return
    elif numDigitado<rangoMin or numDigitado>rangoMax :
        print (" El jugador 2: ""¡",nombreJugador2,", haz fallado por número fuera de rango, no te rindas! ")  
    else:
        print (" El jugador 2: ""¡",nombreJugador2,", haz fallado, no te rindas! ") 
        return

def validarJugador3Perder(numDigitado):
    global penaltiConsecutivoJugador3, cantidadAciertoConsecutivoJugador3,penaltiJugador3,jugador3
    penaltiConsecutivoJugador3 = penaltiConsecutivoJugador3+1
    cantidadAciertoConsecutivoJugador3 = 0
    penaltiJugador3 = penaltiJugador3+1
    global rangoMin, rangoMax

    print("Cantidad de penaltis consecutivos del jugador 3:",str(penaltiConsecutivoJugador3),"   ""Cantidad de penaltis del jugador 3:", str(penaltiJugador3))
    if penaltiConsecutivoJugador3 == 2:
        jugador3 = ""
        #devolver la variable del nombre a "" 
        print (nombreJugador3,": ha perdido el juego con dos penaltis consecutivos")
        print ("          ¡Estas eliminado!           ")
        return
    elif penaltiJugador3 == 3 :
        jugador3 = ""
        print (nombreJugador3,": ha perdido el juego con 3 penaltis en total ")
        print ("          ¡Estas eliminado!           ")
        return
    elif numDigitado<rangoMin or numDigitado>rangoMax :
        print (" El jugador 3: ""¡",nombreJugador3,", haz fallado por número fuera de rango, no te rindas! ")  
    else:
        print (" El jugador 3: ""¡",nombreJugador3,", haz fallado, no te rindas! ") 
        return
           
def validarJugador4Perder(numDigitado):
    global penaltiConsecutivoJugador4, cantidadAciertoConsecutivoJugador4,penaltiJugador4,jugador4
    penaltiConsecutivoJugador4 = penaltiConsecutivoJugador4+1
    cantidadAciertoConsecutivoJugador4 = 0
    penaltiJugador4 = penaltiJugador4+1
    global rangoMin, rangoMax

    print("Cantidad de penaltis consecutivos del jugador 4:",str(penaltiConsecutivoJugador4),"   ""Cantidad de penaltis del jugador 4:", str(penaltiJugador4))
    if penaltiConsecutivoJugador4 == 2:
        jugador4 = ""
        #devolver la variable del nombre a "" 
        print (nombreJugador4,": ha perdido el juego con dos penaltis consecutivos")
        print ("          ¡Estas eliminado!           ")
        return
    elif penaltiJugador4 == 3 :
        jugador4 = ""
        print (nombreJugador4,": ha perdido el juego con 3 penaltis en total ")
        print ("          ¡Estas eliminado!           ")
        return
    elif numDigitado<rangoMin or numDigitado>rangoMax :
        print (" El jugador 4: ""¡",nombreJugador4,", haz fallado por número fuera de rango, no te rindas! ")  
    else:
        print (" El jugador 4: ""¡",nombreJugador4,", haz fallado, no te rindas! ")  
        return

def obtenerNumGanadorUsuario (ingreseNumero, numGanador):
    global jugadorActual
    global penaltiConsecutivoJugador1, penaltiConsecutivoJugador2,penaltiConsecutivoJugador3,penaltiConsecutivoJugador4
    global penaltiJugador1,penaltiJugador2,penaltiJugador3,penaltiJugador4
    global cantidadAciertoConsecutivoJugador1, cantidadAciertoConsecutivoJugador2,cantidadAciertoConsecutivoJugador3,cantidadAciertoConsecutivoJugador4
    global cantidadAciertosJugador1,cantidadAciertosJugador2,cantidadAciertosJugador3,cantidadAciertosJugador4
    global ganador
    global jugador1,jugador2,jugador3,jugador4

    if ingreseNumero == numGanador:
        if jugadorActual==1:
          validarJugador1Ganar()
          return
        elif jugadorActual==2:
          validarJugador2Ganar()
          return
        elif jugadorActual==3:
            validarJugador3Ganar()
            return
        else: 
             validarJugador4Ganar()
             return
    else:
        if jugadorActual==1:
           validarJugador1Perder(ingreseNumero)
           return
        elif jugadorActual==2:
            validarJugador2Perder(ingreseNumero)
            return
        elif jugadorActual==3:
            validarJugador3Perder(ingreseNumero)
            return
        else:
            validarJugador4Perder(ingreseNumero)
            return
 
def riesgoYsuerte ():
    print (reglas ())
    text = input ("Ingrese enter para continuar")
    if text == "":
        print ("Inicio del juego")
        cantidadJugadores ()
    else: 
        (riesgoYsuerte ())

def inicioPartidas():
    print("*********************** Partida iniciada *****************************")
    #setear el jugador actual a la variable
    global ganador
    global jugadorActual
    global jugadores
    jugadorActual = 1
    jugadorSolitario = False
    ganador = False

    while ganador==False:
        jugadorSolitario = validarJugadorSolitario()
        if jugadorActual==1 and jugadorSolitario == False:
            if jugador1!="":
                numDigitado = int(input (jugador1+" Ingrese el numero que cree es el ganador "))
                partidas(numDigitado)
                ganadorUser = jugadorActual
                jugadorActual=jugadorActual+1
            else:
                jugadorActual=jugadorActual+1
                
        elif jugadorActual==2 and jugadorSolitario == False:
            if jugador2!="":
                numDigitado = int(input (jugador2+" Ingrese el numero que cree es el ganador "))
                partidas(numDigitado)
                ganadorUser = jugadorActual
                jugadorActual=jugadorActual+1
            else:
                jugadorActual=jugadorActual+1
        elif jugadorActual==3 and jugadorSolitario == False:
            if jugador3!="":
                numDigitado = int(input (jugador3+" Ingrese el numero que cree es el ganador "))
                partidas(numDigitado)
                ganadorUser = jugadorActual 
                jugadorActual=jugadorActual+1
            else:
                jugadorActual=jugadorActual+1
        elif jugadorActual==4 and jugadorSolitario == False:
            if jugador4!="":
                numDigitado = int(input (jugador4+" Ingrese el numero que cree es el ganador "))
                partidas(numDigitado)
                ganadorUser = jugadorActual
                jugadorActual=1
            else:
                ganadorUser = jugadorActual
                jugadorActual=1
        else:
            printUsuariosStatus(jugadorActual)
            break
            
    if ganador == True or jugadorSolitario == True:
        printUsuariosStatus(ganadorUser)

def partidas(numDigitado):
    global jugadorActual
    numGanador = calcularNumeroGanador(numDigitado)
    obtenerNumGanadorUsuario(numDigitado,numGanador)

def printResultadosUser1():
    global nombreJugador1,penaltiConsecutivoJugador1,penaltiJugador1,cantidadAciertoConsecutivoJugador1,cantidadAciertosJugador1
    print("Para el jugador: "+ nombreJugador1 +
        "\n Numero de penalties consecutivos: " + str(penaltiConsecutivoJugador1) + 
        "\n Numero de penalties: " + str(penaltiJugador1) + 
        "\n Numero de aciertos: "+ str(cantidadAciertosJugador1) + 
        "\n Numero de aciertos consecutivos: "+ str(cantidadAciertoConsecutivoJugador1))

def printResultadosUser2():
    global nombreJugador2,penaltiConsecutivoJugador2,penaltiJugador2,cantidadAciertoConsecutivoJugador2,cantidadAciertosJugador2
    print("Para el jugador: "+ nombreJugador2 +
        "\n Numero de penalties consecutivos: " + str(penaltiConsecutivoJugador2) + 
        "\n Numero de penalties: " + str(penaltiJugador2) + 
        "\n Numero de aciertos: "+ str(cantidadAciertosJugador2) + 
        "\n Numero de aciertos consecutivos: "+ str(cantidadAciertoConsecutivoJugador2))

def printResultadosUser3():
    global nombreJugador3,penaltiConsecutivoJugador3,penaltiJugador3,cantidadAciertoConsecutivoJugador3,cantidadAciertosJugador3
    print("Para el jugador: "+ nombreJugador3 +
        "\n Numero de penalties consecutivos: " + str(penaltiConsecutivoJugador3) + 
        "\n Numero de penalties: " + str(penaltiJugador3) + 
        "\n Numero de aciertos: "+ str(cantidadAciertosJugador3) + 
        "\n Numero de aciertos consecutivos: "+ str(cantidadAciertoConsecutivoJugador3))

def printResultadosUser4():
    global nombreJugador4,penaltiConsecutivoJugador4,penaltiJugador4,cantidadAciertoConsecutivoJugador4,cantidadAciertosJugador4
    print("Para el jugador: "+ nombreJugador4 +
        "\n Numero de penalties consecutivos: " + str(penaltiConsecutivoJugador4) + 
        "\n Numero de penalties: " + str(penaltiJugador4) + 
        "\n Numero de aciertos: "+ str(cantidadAciertosJugador4) + 
        "\n Numero de aciertos consecutivos: "+ str(cantidadAciertoConsecutivoJugador4))

def printUsuariosPerdedores(userWinner):
    global jugadores
   
    if jugadores==2:
        if userWinner==1:
            printResultadosUser2()
        else:
            printResultadosUser1()
    elif jugadores==3:
        if userWinner==1 :
            if cantidadAciertosJugador2>=cantidadAciertosJugador3:
                printResultadosUser2()
                printResultadosUser3()
            else:
                printResultadosUser3()
                printResultadosUser2()
        elif userWinner==2:
            if cantidadAciertosJugador1>=cantidadAciertosJugador3:
                printResultadosUser1()
                printResultadosUser3()
            else:
                printResultadosUser3()
                printResultadosUser1()
        else:
            if cantidadAciertosJugador1>=cantidadAciertosJugador2:
                printResultadosUser1()
                printResultadosUser2()
            else:
                printResultadosUser2()
                printResultadosUser1()

    else:
        if userWinner==1:
            
            if cantidadAciertosJugador2>cantidadAciertosJugador3 and cantidadAciertosJugador2>cantidadAciertosJugador4:
                print("1"+str(userWinner))
                printResultadosUser2()
                if cantidadAciertosJugador3>=cantidadAciertosJugador4:
                    printResultadosUser3()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser3()
            elif cantidadAciertosJugador3>cantidadAciertosJugador2 and cantidadAciertosJugador3>cantidadAciertosJugador4:
                printResultadosUser3()
                if cantidadAciertosJugador2>=cantidadAciertosJugador4:
                    printResultadosUser2()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser2()
            else:
                printResultadosUser4()
                if cantidadAciertosJugador2>=cantidadAciertosJugador3:
                    printResultadosUser2()
                    printResultadosUser3()
                else:
                    printResultadosUser3()
                    printResultadosUser2()

                
        elif userWinner==2:
            
            if cantidadAciertosJugador1>cantidadAciertosJugador3 and cantidadAciertosJugador1>cantidadAciertosJugador4:
                print("1"+str(userWinner))
                printResultadosUser1()
                if cantidadAciertosJugador3>=cantidadAciertosJugador4:
                    printResultadosUser3()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser3()
            elif cantidadAciertosJugador3>cantidadAciertosJugador1 and cantidadAciertosJugador3>cantidadAciertosJugador4:
                printResultadosUser3()
                if cantidadAciertosJugador1>=cantidadAciertosJugador4:
                    printResultadosUser1()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser1()
            else:
                printResultadosUser4()
                if cantidadAciertosJugador1>=cantidadAciertosJugador3:
                    printResultadosUser1()
                    printResultadosUser3()
                else:
                    printResultadosUser3()
                    printResultadosUser1()
        elif userWinner==3:
            
            if cantidadAciertosJugador1>cantidadAciertosJugador2 and cantidadAciertosJugador1>cantidadAciertosJugador4:
                print("1"+str(userWinner))
                printResultadosUser1()
                if cantidadAciertosJugador2>=cantidadAciertosJugador4:
                    printResultadosUser2()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser2()
            elif cantidadAciertosJugador2>cantidadAciertosJugador1 and cantidadAciertosJugador2>cantidadAciertosJugador4:
                printResultadosUser2()
                if cantidadAciertosJugador1>=cantidadAciertosJugador4:
                    printResultadosUser1()
                    printResultadosUser4()
                else:
                    printResultadosUser4()
                    printResultadosUser1()
            else:
                printResultadosUser4()
                if cantidadAciertosJugador1>=cantidadAciertosJugador2:
                    printResultadosUser1()
                    printResultadosUser2()
                else:
                    printResultadosUser2()
                    printResultadosUser1()
        else:
            
            if cantidadAciertosJugador1>cantidadAciertosJugador2 and cantidadAciertosJugador1>cantidadAciertosJugador3:
                print("1"+str(userWinner))
                printResultadosUser1()
                if cantidadAciertosJugador2>=cantidadAciertosJugador3:
                    printResultadosUser2()
                    printResultadosUser3()
                else:
                    printResultadosUser3()
                    printResultadosUser2()
            elif cantidadAciertosJugador2>cantidadAciertosJugador1 and cantidadAciertosJugador2>cantidadAciertosJugador3:
                printResultadosUser2()
                if cantidadAciertosJugador1>=cantidadAciertosJugador3:
                    printResultadosUser1()
                    printResultadosUser3()
                else:
                    printResultadosUser3()
                    printResultadosUser1()
            else:
                printResultadosUser3()
                if cantidadAciertosJugador1>=cantidadAciertosJugador2:
                    printResultadosUser1()
                    printResultadosUser2()
                else:
                    printResultadosUser2()
                    printResultadosUser1()

def printUsuariosStatus(userWinner):
     # Imprimir status de los jugadores
    if userWinner==1:
        print("\n \n********************** Ganador: *************************")
        printResultadosUser1()
        print("********************** Perdedores:***********************")
        printUsuariosPerdedores(userWinner)
        ganador=False
        validarContinuar()
    elif userWinner==2:
        print("********************** Ganador: *************************")
        printResultadosUser2()
        print("********************** Perdedores:***********************")
        printUsuariosPerdedores(userWinner)
        ganador=False
        validarContinuar()
    elif userWinner==3:
        print("********************** Ganador: *************************")
        printResultadosUser3()
        print("********************** Perdedores:***********************")
        printUsuariosPerdedores(userWinner)
        ganador=False
        validarContinuar()
    else:
        print("********************** Ganador: *************************")
        printResultadosUser4()
        print("********************** Perdedores:***********************")
        printUsuariosPerdedores(userWinner)
        ganador=False
        validarContinuar()

def validarJugadorSolitario():
    # Si ya perdieron todos y queda solo 1 jugador (Gana por default)
    global jugador1,jugador2,jugador3,jugador4, jugadorActual
    if jugadorActual==1 and (jugador2=="" and jugador3=="" and jugador4==""):
        return True
    elif jugadorActual==2 and (jugador1=="" and jugador3=="" and jugador4==""):
        return True
    elif jugadorActual==3 and (jugador1=="" and jugador2=="" and jugador4==""):
        return True
    elif jugadorActual==4 and (jugador1=="" and jugador2=="" and jugador3==""):
        return True
    else:
        return False

def limpiarCampos():
    global penaltiJugador1, penaltiJugador2,penaltiJugador3, penaltiJugador4
    global penaltiConsecutivoJugador1, penaltiConsecutivoJugador2, penaltiConsecutivoJugador3, penaltiConsecutivoJugador4
    global ganador, jugadores, jugadorActual

    penaltiJugador1 = 0
    penaltiJugador2 = 0
    penaltiJugador3 = 0
    penaltiJugador4 = 0
    penaltiConsecutivoJugador1 = 0
    penaltiConsecutivoJugador2 = 0
    penaltiConsecutivoJugador3 = 0
    penaltiConsecutivoJugador4 = 0
    ganador = False
    jugadores = 0
    jugadorActual = 0

def validarContinuar():
    text = input ("Ingrese enter para continuar con una nueva partida")
    if text == "":
        limpiarCampos()
        print ("Inicio del juego")
        cantidadJugadores ()
    else: 
        print ("************************FIN*************************")
        